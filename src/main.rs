use anyhow::Result;
use chrono::{naive::NaiveTime, DateTime, Local};
use clap::Clap;
use dmx::{self, DmxTransmitter};
use iso8601_duration::Duration as IsoDuration;
use std::convert::TryInto;
use std::thread;
use std::time::Duration;
use thiserror::Error;

#[derive(Clap, Debug)]
#[clap(name = "basic")]
struct Opts {
    /// On a scale of 0-99, with 99 being the warmest light temp possible
    /// what should it be?
    #[clap(long = "night-temp", default_value = "99")]
    night_temp: u16,

    /// On a scale of 0-99, with 99 being the coolest light temp possible
    /// what should it be?
    #[clap(long = "day-temp", default_value = "30")]
    day_temp: u16,

    /// The (local) time in which you want the color temp to be the coolest
    /// Uses ISO time format, e.g. 22:00:00
    #[clap(long, default_value = "20:00:00")]
    sunset: NaiveTime,

    #[clap(long = "warm-up", default_value = "PT6h")]
    warm_up: IsoDuration,

    /// The time-step in which daylight will adjust the lighting temp during the
    /// warm-up and cool-down periods
    /// This follows the "ISO-8601 Duration" notation
    /// e.g. PT30m to update the color every 30 minutes
    #[clap(short, long)]
    increment: IsoDuration,

    /// The (local) time in which you want the color temp to be the coolest
    /// Uses ISO time format, e.g. 06:00:00
    #[clap(long, default_value = "06:00:00")]
    sunrise: NaiveTime,

    #[clap(long = "cool-down", default_value = "PT2h")]
    cool_down: IsoDuration,

    #[clap(short, long = "tty", default_value = "/dev/ttyUSB0")]
    tty: String,

    #[clap(long, default_value = "99")]
    brightness: u16,
}

fn main() -> Result<()> {
    let opts = Opts::parse();
    let mut dmx_port = dmx::open_serial(&opts.tty).expect("accessing serial port");
    let mut sched: Scheduler = opts.try_into()?;

    // a typical 4-channel device, expecting RGBV values. this will set a
    // fairly bright yellow.
    loop {
        let now = Local::now();
        let (task, next) = sched.timecheck(now)?;
        if let Some(t) = task {
            sched.run_task(t, &mut dmx_port)?;
        }
        thread::sleep(next);
    }
}

#[derive(Clone, Copy, Debug)]
struct ScheduleTask {
    start: Duration,
    rgbv: [u8; 4],
}

impl ScheduleTask {
    pub fn new(start: Duration, r: u16, g: u16, b: u16, v: u16) -> ScheduleTask {
        ScheduleTask {
            start,
            rgbv: [r as u8, g as u8, b as u8, v as u8],
        }
    }
}

struct Scheduler {
    schedule: Vec<ScheduleTask>,
}

impl Scheduler {
    // This is our wake-up state check
    // We could just blindly follow the pre-scheduled durations, but
    // instead we spend a few extra cycles and get the wall time
    // and find the appropriate scheduled task for that time.
    // This helps protect against the drift that would occur if we were only
    // using sleep(duration)
    fn timecheck(&self, now: DateTime<Local>) -> Result<(Option<ScheduleTask>, Duration)> {
        // get seconds since midnight
        let midnight = now.date().and_hms(0, 0, 0);
        let since_midnight = now.signed_duration_since(midnight).to_std()?;
        dbg!("since_midnight: {}", since_midnight);
        // see where we are in the schedule
        let (idx, task) = self
            .schedule
            .windows(2)
            .enumerate()
            .find_map(|(i, ref tasks)| {
                let t1 = &tasks[0];
                let t2 = &tasks[1];
                if t1.start < since_midnight && t2.start > since_midnight {
                    Some((i, t1))
                } else {
                    None
                }
            })
            .unwrap_or((0, self.schedule.last().unwrap()));

        let next_idx = idx + 1 % self.schedule.len();
        let next_task = self.schedule[next_idx];
        let span = if since_midnight > next_task.start {
            Duration::from_secs(24 * 3600) - since_midnight + next_task.start
        } else {
            next_task.start - since_midnight
        };
        Ok((Some(*task), span))
    }

    fn run_task<T: DmxTransmitter>(&mut self, task: ScheduleTask, xmit: &mut T) -> Result<()> {
        // Superfluous ? and Ok to make the typechecker happy with anyhow
        Ok(xmit.send_dmx_packet(&task.rgbv)?)
    }

    fn make_schedule(opts: &Opts) -> Result<Vec<ScheduleTask>> {
        let midnight = NaiveTime::from_hms(0, 0, 0);
        let sunrise = opts.sunrise.signed_duration_since(midnight).to_std()?;
        let sunset = opts.sunset.signed_duration_since(midnight).to_std()?;
        let cool_down = opts.cool_down.to_std();
        let warm_up = opts.warm_up.to_std();
        let cool_down_start = sunrise - cool_down;
        let warm_up_start = sunset - warm_up;
        let night_red_target = 255_u16 * opts.night_temp / 100;
        let night_blue_target = 255_u16 * (99 - opts.night_temp) / 100;
        let day_blue_target = 255_u16 * opts.day_temp / 100;
        let day_red_target = 255_u16 * (99 - opts.day_temp) / 100;
        let brightness = 255_u16 * opts.brightness / 100;
        let blue_range = day_blue_target - night_blue_target;
        let red_range = night_red_target - day_red_target;
        let increment = opts.increment.to_std();
        let increment_sec = increment.as_secs();
        let num_warm_up_increments = warm_up.as_secs() / increment_sec;
        let num_cool_down_increments = cool_down.as_secs() / increment_sec;
        let warm_up_step_red = red_range / num_warm_up_increments as u16;
        let cool_down_step_red = red_range / num_cool_down_increments as u16;
        let warm_up_step_blue = blue_range / num_warm_up_increments as u16;
        let cool_down_step_blue = blue_range / num_cool_down_increments as u16;
        let mut schedule = vec![ScheduleTask::new(
            Duration::from_secs(0),
            night_red_target,
            0,
            night_blue_target,
            brightness,
        )];
        let mut i = 0_u16;
        let mut dur = cool_down_start + (increment * i as u32);
        while dur < sunrise {
            i = i + 1;
            let tsk = ScheduleTask::new(
                dur,
                night_red_target - (cool_down_step_red * i + 1),
                0,
                night_blue_target + (cool_down_step_blue * i + 1),
                brightness,
            );
            schedule.push(tsk);
            dur = cool_down_start + (increment * i as u32);
        }
        schedule.push(ScheduleTask::new(
            sunrise,
            day_red_target,
            0,
            day_blue_target,
            brightness,
        ));
        i = 0;
        dur = warm_up_start + (increment * i as u32);
        while dur < sunset {
            i = i + 1;
            schedule.push(ScheduleTask::new(
                dur,
                day_red_target + (warm_up_step_red * i),
                0,
                day_blue_target - (warm_up_step_blue * i),
                brightness,
            ));
            dur = warm_up_start + (increment * i as u32);
        }
        schedule.push(ScheduleTask::new(
            sunset,
            night_red_target,
            0,
            night_blue_target,
            brightness,
        ));
        Ok(schedule)
    }
}

impl TryInto<Scheduler> for Opts {
    type Error = anyhow::Error;
    fn try_into(self) -> Result<Scheduler, Self::Error> {
        Ok(Scheduler {
            schedule: Scheduler::make_schedule(&self)?,
        })
    }
}

#[derive(Error, Debug)]
pub enum SchedulerError {
    #[error("invalid index into schedule {0:?}")]
    InvalidIndex(Option<usize>),
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use chrono::TimeZone;
    fn make_schedule() -> Vec<ScheduleTask> {
        let opts = Opts {
            night_temp: 90,
            day_temp: 70,
            sunset: "21:00:00".parse().unwrap(),
            warm_up: "PT2H".parse().unwrap(),
            increment: "PT20M".parse().unwrap(),
            sunrise: "07:00:00".parse().unwrap(),
            cool_down: "PT2H".parse().unwrap(),
            tty: "/dev/tty1".into(),
            brightness: 80,
        };
        let sched = Scheduler::make_schedule(&opts).expect("make schedule");
        assert_eq!(sched[4].start, Duration::from_secs(21600));
        assert_eq!(&sched[4].rgbv, &[124, 0, 127, 204]);

        for s in sched.iter() {
            dbg!("{:?}", s);
        }
        sched
    }

    #[test]
    fn test_run() {
        let schedule = make_schedule();
        let scheduler = Scheduler { schedule };
        let now = Local.ymd(2020, 1, 1).and_hms(18, 10, 0);
        let (task, dur) = scheduler.timecheck(now).expect("timecheck");
        let tsk = task.unwrap();
        assert_eq!(tsk.start, Duration::from_secs(25200));
        assert_eq!(&tsk.rgbv, &[73, 0, 178, 204]);
        assert_eq!(dur, Duration::from_secs(3000));
    }
}

/*
dbg!(
    r#"
    sunrise: {},
    sunset: {},
    cool_down: {},
    warm_up: {},
    increment: {},
    blue_range: {},
    red_range: {}
    night_red_target: {}
    night_blue_target: {},
    day_red_target: {},
    day_blue_target: {},
    num_warm_up_increments: {},
    num_cool_down_increments: {},
    warm_up_step_red: {},
    cool_down_step_red: {},
    warm_up_step_blue: {},
    cool_down_step_blue: {}
    "#,
    sunrise,
    sunset,
    cool_down,
    warm_up,
    increment,
    blue_range,
    red_range,
    night_red_target,
    night_blue_target,
    day_red_target,
    day_blue_target,
    num_warm_up_increments,
    num_cool_down_increments,
    warm_up_step_red,
    cool_down_step_red,
    warm_up_step_blue,
    cool_down_step_blue
);
*/
